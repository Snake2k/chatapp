package chatapp;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.JTextArea;

public class ChatClient implements Runnable{
	private InputStream inputFromServer;
	private OutputStream outputToServer;
	private Socket socket;
	private int ID;
	private JTextArea outputPanel;
	private boolean hasUI = false;

	Queue<Message> messages;
	
	public ChatClient(){
		messages = new LinkedList<>();
		this.ID = ClientHandler.getAndIncrementIDCounter();
	}
	
	public ChatClient(Socket socket){
		this();
		this.socket = socket;
		try {
			inputFromServer = socket.getInputStream();
			outputToServer = socket.getOutputStream();	
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	@Override
	public void run() {
		try {
			byte[] buffer = new byte[1024];
			inputFromServer.read(buffer);
			Message receivedMessage = Networking.deserializeMessage(new String(buffer).trim());
			System.out.println("Username: " + receivedMessage.getContents() + " ID: " + ID);
			if(!hasUI) return;
			outputPanel.append(receivedMessage.getContents() + " has connected\n");
			while(true){
				inputFromServer.read(buffer);
				String serializedMessage = new String(buffer).trim();
				outputPanel.append(Networking.deserializeMessage(serializedMessage).getContents() + "\n");
				Arrays.fill(buffer, " ".getBytes()[0]);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void connectToServer(String hostName, int port){
		try {
			socket = new Socket(hostName, port);
			inputFromServer = socket.getInputStream();
			outputToServer = socket.getOutputStream();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	public boolean hasNewMessage(){
		return messages.size() > 0;
	}
	
	public Message getNewMessage(){
		return messages.remove();
	}
	
	public void addMessage(Message message){
		messages.add(message);
	}
	public int getID() {
		return ID;
	}

	public Socket getSocket() {
		return socket;
	}

	public OutputStream getOutputStream() {
		return outputToServer;
	}

	public void setOutputPanel(JTextArea outputPanel) {
		this.outputPanel = outputPanel;
	}

	public boolean isHasUI() {
		return hasUI;
	}

	public void setHasUI(boolean hasUI) {
		this.hasUI = hasUI;
	}
	
}
