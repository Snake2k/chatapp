package chatapp;

import java.io.*;
import java.net.*;
import java.util.*;

public class ChatServer implements Runnable{
	private ServerSocket serverSocket;
	private List<ChatClient> connectedClients;
	Queue<Message> messages;
	
	public ChatServer(int port){
		try {
			setServerSocket(new ServerSocket(port));
			connectedClients = new LinkedList<>();
			messages = new LinkedList<>();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		Thread t = new Thread(new ClientHandler(this));
		t.start();
		while(true){
			while(hasNewMessage()){
				Message currentMessage = messages.poll();
				for(ChatClient chatClient : connectedClients){
					if(currentMessage.isPublic() || currentMessage.getRecipientID() == chatClient.getID()) {
						Networking.sendMessage(currentMessage, chatClient.getSocket());		
					}
				}
				System.out.println("From server, received message: " + currentMessage.getContents());
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public boolean hasNewMessage(){
		return messages.size() > 0;
	}
	
	public Message getNewMessage(){
		return messages.remove();
	}
	
	public void addMessage(Message message){
		messages.add(message);
	}
	
	public void addClient(ChatClient client){
		connectedClients.add(client);		
	}

	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	public void setServerSocket(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}
}
