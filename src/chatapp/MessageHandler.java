package chatapp;

import java.io.*;
import java.net.*;

import com.google.gson.Gson;

public class MessageHandler implements Runnable{
	private InputStream inputFromServer;
	private ChatServer server;
	private Gson gson;
	
	public MessageHandler(Socket socket, ChatServer server){
		this.server = server;
		gson = new Gson();
		try {
			inputFromServer = socket.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		while(true){
			try {
				byte[] buffer = new byte[1024];
				inputFromServer.read(buffer);
				if(buffer[0] != '{') continue;
				Message message = gson.fromJson(new String(buffer).trim(),  Message.class);
				server.addMessage(message);
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
	}

}
